### SUMMARY
This module provides configurable access conditions for field groups as page manager module style.

### INSTALLATION
Using composer:
```
composer require drupal/access_conditions_field_group --sort-packages
```

### RECOMMENDED MODULES
Condition plugins: https://www.drupal.org/project/condition_plugins

### ALTERNATIVE MODULES
Field Group Access / Permissions: https://www.drupal.org/project/field_group_access

### SPONSORS
- [Fundación UNICEF Comité Español](https://www.unicef.es)

### CONTACT
Developed and maintained by Cambrico (http://cambrico.net).

Get in touch with us for customizations and consultancy:
http://cambrico.net/contact

#### Current maintainers:
- Pedro Cambra [(pcambra)](https://www.drupal.org/u/pcambra)
- Manuel Egío [(facine)](https://www.drupal.org/u/facine)
